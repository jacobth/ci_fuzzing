#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "c_script.c"
/*
int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size){
  printf("%s\n", data);
  return 0;
}
*/
extern "C" int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
  // Create a c string from fuzzer data (with an additional byte set to '\0')
  char *str = (char *)malloc(sizeof(char) * size + 1); // Create a c-string of length size + 1
  memcpy(str, data, size);                             // Copy fuzzer data to string
  str[size] = '\0';                                    // Set last byte of allocated string to '\0'

  //assert(strstr(checkString(str), "&amp") != NULL)
  char *string = checkString(str);
  free(string);
  free(str);

  return 0;
}


